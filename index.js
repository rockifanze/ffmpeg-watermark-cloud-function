'use strict';

const path = require('path');
const os = require('os');
const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const ffmpeg_static = require('ffmpeg-static');

function promisifyCommand(command) {
    return new Promise((resolve, reject) => {
        command
            .on('end', () => {
                console.log("fluent-ffmpeg finish");
                resolve();
            })
            .on('error', (error) => {
                console.log("fluent-ffmpeg", error.message);
                reject(error);
            })
            .run();
    });
}


// Get the file name.
const fileName = path.basename("ffmpeg-test/rs.mp4");
// Exit if the video is already converted.
if (fileName.endsWith('_output.mp4')) {
    console.log('Already a converted video.');
    return null;
}

//Water Mark file from bucket.
const waterMarkFileName = 'ic_launcher.png';
const waterMarkTempFilePath = path.join("ffmpeg-test/", waterMarkFileName);

// Download file from bucket.const bucket = gcs.bucket(fileBucket);
const tempFilePath = path.join("ffmpeg-test/", fileName);
// We add a '_output.flac' suffix to target video file name. That's where we'll upload the converted video.
const targetTempFileName = fileName.replace(/\.[^/.]+$/, '') + '_output.mp4';
const targetTempFilePath = path.join("ffmpeg-test/", targetTempFileName);
const targetStorageFilePath = path.join(path.dirname(fileName), targetTempFileName);
// Convert the video to mono channel using FFMPEG.
console.log(tempFilePath, waterMarkTempFilePath, targetTempFilePath);
let command = ffmpeg(tempFilePath)
    .setFfmpegPath(ffmpeg_static.path)
    .addInput(waterMarkTempFilePath)
    .complexFilter(['overlay=0.0'])
    .output(targetTempFilePath);

promisifyCommand(command)
    .then(() => {
        console.log('Output video created at', targetTempFilePath);
        // Uploading the video.
        console.log('Output video uploaded to', targetStorageFilePath);

        // Once the video has been uploaded delete the local file to free up disk space.
        // fs.unlinkSync(tempFilePath);
        // fs.unlinkSync(targetTempFilePath);

        return console.log('Temporary files removed.', targetTempFilePath);
    });